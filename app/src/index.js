const express = require('express')
const app = express()

//Middleware
app.use(express.json())

//routes
app.get('/', (req, res) => {
    const msg = 'Hello World!'
    res.json({ msg })
})

module.exports = app